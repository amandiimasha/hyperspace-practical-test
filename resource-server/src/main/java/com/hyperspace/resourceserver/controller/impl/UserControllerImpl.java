package com.hyperspace.resourceserver.controller.impl;

import com.hyperspace.resourceserver.controller.UserController;
import com.hyperspace.resourceserver.dto.request.UserCreateDto;
import com.hyperspace.resourceserver.dto.request.UserUpdateDto;
import com.hyperspace.resourceserver.dto.response.ListResponse;
import com.hyperspace.resourceserver.dto.response.UserDto;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.model.User;
import com.hyperspace.resourceserver.service.custom.UserService;
import com.hyperspace.resourceserver.util.DtoConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @author Amandi Imasha
 */
@RestController
@Slf4j
public class UserControllerImpl implements UserController {

    @Autowired
    private UserService userService;

    @Override
    public UserDto createUser(UserCreateDto requestDto) {

        User save = userService.save(DtoConverter.convertToUser(requestDto));
        return DtoConverter.convertToUserDto(save);
    }

    @Override
    public UserDto getUser(String id) {
        Optional<User> user = userService.findById(id);
        if (user.isPresent()){
            return DtoConverter.convertToUserDto(user.get());
        }else {
            throw new RecordNotFoundException("can not found user by given id : "+id);
        }
    }

    @Override
    public UserDto updateUser(String id, UserUpdateDto requestDto) {
        return null;
    }

    @Override
    public UserDto deleteUser(String id) {
        User user = userService.delete(id);
        return DtoConverter.convertToUserDto(user);
    }

    @Override
    public ListResponse<UserDto> getUsers(Integer offset, Integer limit) {
        Page<User> paginatedList = userService.getPaginatedList(offset, limit);
        return ListResponse.<UserDto>builder()
                .size(paginatedList.getSize())
                .noOfPages(paginatedList.getTotalPages())
                .noOfElements(paginatedList.getNumberOfElements())
                .totalElements(paginatedList.getTotalElements())
                .data(DtoConverter.convertToDtoList(paginatedList.getContent()))
                .build();
    }
}
