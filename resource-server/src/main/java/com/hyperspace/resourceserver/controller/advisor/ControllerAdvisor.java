package com.hyperspace.resourceserver.controller.advisor;

import com.hyperspace.resourceserver.exception.DuplicateRecordException;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.exception.SystemException;
import com.hyperspace.resourceserver.util.ResponseErrorWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author Amandi Imasha
 */
@Slf4j
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler({RecordNotFoundException.class})
    protected ResponseEntity<ResponseErrorWrapper> handleRecodeNotFoundApiCall(RecordNotFoundException e) {
        ResponseErrorWrapper response = ResponseErrorWrapper.builder()
                .code(e.getErrorCode())
                .message(e.getMessage())
                .status(HttpStatus.NOT_FOUND)
                .errors(e.getMessage()).build();
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DuplicateRecordException.class})
    protected ResponseEntity<ResponseErrorWrapper> handleDuplicateRecodeApiCall(DuplicateRecordException e) {
        ResponseErrorWrapper response = ResponseErrorWrapper.builder()
                .code(e.getErrorCode())
                .message(e.getMessage())
                .status(HttpStatus.BAD_REQUEST)
                .errors(e.getMessage()).build();
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({SystemException.class})
    protected ResponseEntity<ResponseErrorWrapper> handleSystemException(SystemException e) {
        ResponseErrorWrapper response = ResponseErrorWrapper.builder()
                .code(e.getErrorCode())
                .message(e.getMessage())
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .errors(e.getMessage()).build();
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
