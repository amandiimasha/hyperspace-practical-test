package com.hyperspace.resourceserver.controller;

import com.hyperspace.resourceserver.dto.request.UserCreateDto;
import com.hyperspace.resourceserver.dto.request.UserUpdateDto;
import com.hyperspace.resourceserver.dto.response.ListResponse;
import com.hyperspace.resourceserver.dto.response.UserDto;
import org.springframework.web.bind.annotation.*;

public interface UserController {

    @PostMapping(path = "/api/users")
    public UserDto createUser(@RequestBody UserCreateDto requestDto);

    @GetMapping(path = "/api/users/{id}")
    public UserDto getUser(@PathVariable String id);

    @PutMapping(path = "/api/users/{id}")
    public UserDto updateUser(@PathVariable String id, @RequestBody UserUpdateDto requestDto);

    @DeleteMapping(path = "/api/users/{id}")
    public UserDto deleteUser(@PathVariable String id);

    @GetMapping(path = "/api/users")
    public ListResponse<UserDto> getUsers(@RequestParam(value = "offset", defaultValue = "0") Integer offset,
                                          @RequestParam(value = "limit", defaultValue = "10") Integer limit);
}
