package com.hyperspace.resourceserver.controller.impl;

import com.hyperspace.resourceserver.controller.CategoryController;
import com.hyperspace.resourceserver.dto.request.CategoryCreateDto;
import com.hyperspace.resourceserver.dto.request.CategoryUpdateDto;
import com.hyperspace.resourceserver.dto.response.CategoryDto;
import com.hyperspace.resourceserver.dto.response.ListResponse;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.model.Advertising;
import com.hyperspace.resourceserver.model.Category;
import com.hyperspace.resourceserver.service.custom.AdvertingService;
import com.hyperspace.resourceserver.service.custom.CategoryService;
import com.hyperspace.resourceserver.util.DtoConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Amandi Imasha
 */
@RestController
@Slf4j
public class CategoryControllerImpl implements CategoryController {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AdvertingService advertingService;

    @Override
    public CategoryDto createCategory(CategoryCreateDto requestDto) {
        Category save = categoryService.save(DtoConverter.convertToCategory(requestDto));
        return DtoConverter.convertToCategoryDto(save, new ArrayList<>());
    }

    @Override
    public CategoryDto getCategory(Long id) {
        Optional<Category> found = categoryService.findById(id);
        if (found.isPresent()) {
            List<Advertising> addList = advertingService.getAdvertisingByCategory(id);
            return DtoConverter.convertToCategoryDto(found.get(), addList);

        } else {
            throw new RecordNotFoundException("can not found category by given id : " + id);
        }
    }

    @Override
    public CategoryDto updateCategory(Long id, CategoryUpdateDto requestDto) {
        Category update = categoryService.update(id, DtoConverter.convertToCategory(requestDto));
        return DtoConverter.convertToCategoryDto(update, new ArrayList<>());
    }

    @Override
    public CategoryDto deleteCategory(Long id) {
        Category category = categoryService.delete(id);
        return DtoConverter.convertToCategoryDto(category, new ArrayList<>());
    }

    @Override
    public ListResponse<CategoryDto> getCategories(Integer offset, Integer limit) {
        Page<Category> paginatedList = categoryService.getPaginatedList(offset, limit);
        List<CategoryDto> results = new ArrayList<>();
        for (Category c : paginatedList.getContent()) {
            results.add(DtoConverter
                    .convertToCategoryDto(c, advertingService.getAdvertisingByCategory(c.getId())));
        }

        return ListResponse.<CategoryDto>builder()
                .totalElements(paginatedList.getTotalElements())
                .size(paginatedList.getSize())
                .noOfElements(paginatedList.getNumberOfElements())
                .noOfPages(paginatedList.getTotalPages())
                .data(results)
                .build();
    }
}
