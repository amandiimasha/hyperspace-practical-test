package com.hyperspace.resourceserver.controller;

import com.hyperspace.resourceserver.dto.request.CategoryCreateDto;
import com.hyperspace.resourceserver.dto.request.CategoryUpdateDto;
import com.hyperspace.resourceserver.dto.response.CategoryDto;
import com.hyperspace.resourceserver.dto.response.ListResponse;
import org.springframework.web.bind.annotation.*;

public interface CategoryController {
    @PostMapping(path = "/api/categories")
    public CategoryDto createCategory(@RequestBody CategoryCreateDto requestDto);

    @GetMapping(path = "/api/categories/{id}")
    public CategoryDto getCategory(@PathVariable Long id);

    @PutMapping(path = "/api/categories/{id}")
    public CategoryDto updateCategory(@PathVariable Long id, @RequestBody CategoryUpdateDto requestDto);

    @DeleteMapping(path = "/api/categories/{id}")
    public CategoryDto deleteCategory(@PathVariable Long id);

    @GetMapping(path = "/api/categories")
    public ListResponse<CategoryDto> getCategories(@RequestParam(value = "offset", defaultValue = "0") Integer offset,
                                                   @RequestParam(value = "limit", defaultValue = "10") Integer limit);
}
