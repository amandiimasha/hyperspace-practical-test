package com.hyperspace.resourceserver.controller;

import com.hyperspace.resourceserver.dto.request.CreateAdvertisingDto;
import com.hyperspace.resourceserver.dto.request.UpdateAdvertisingDto;
import com.hyperspace.resourceserver.dto.response.AdvertisementDto;
import com.hyperspace.resourceserver.dto.response.ListResponse;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

public interface AdvertisementController {

    @PostMapping(path = "/api/categories/{category_id}/advertisings", consumes = "application/json")
    public AdvertisementDto createAdvertising(@PathVariable("category_id") Long categoryId,
                                              @RequestBody @Valid CreateAdvertisingDto requestDto);

    @GetMapping(path = "/api/categories/{category_id}/advertisings/{add_id}")
    public AdvertisementDto getAdvertising(@PathVariable("category_id") Long categoryId,
                                           @PathVariable("add_id") Long addId);

    @PutMapping(path = "/api/categories/{category_id}/advertisings/{add_id}")
    public AdvertisementDto updateAdvertising(@PathVariable("category_id") Long categoryId,
                                              @PathVariable("add_id") Long addId,
                                              @RequestBody @Valid UpdateAdvertisingDto requestDto);

    @DeleteMapping(path = "/api/categories/{category_id}/advertisings/{add_id}")
    public AdvertisementDto deleteAdvertising(@PathVariable("category_id") Long categoryId,
                                              @PathVariable("add_id") Long addId);

    @GetMapping(path = "/api/categories/{category_id}/advertisings")
    public ListResponse<AdvertisementDto> getAdvertisings(
            @PathVariable("category_id") Long categoryId,
            @RequestParam(value = "offset", defaultValue = "0") Integer offset,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit);

    @GetMapping(path = "/api/advertisings")
    public ListResponse<AdvertisementDto> getAllAdvertisings(
            @RequestParam(value = "offset", defaultValue = "0") Integer offset,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit);
}
