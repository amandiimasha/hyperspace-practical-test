package com.hyperspace.resourceserver.controller.impl;

import com.hyperspace.resourceserver.controller.AdvertisementController;
import com.hyperspace.resourceserver.dto.request.CreateAdvertisingDto;
import com.hyperspace.resourceserver.dto.request.UpdateAdvertisingDto;
import com.hyperspace.resourceserver.dto.response.AdvertisementDto;
import com.hyperspace.resourceserver.dto.response.ListResponse;
import com.hyperspace.resourceserver.model.Advertising;
import com.hyperspace.resourceserver.service.custom.AdvertingService;
import com.hyperspace.resourceserver.util.DtoConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Amandi Imasha
 */
@RestController
@Slf4j
public class AdvertisementControllerImpl implements AdvertisementController {

    @Autowired
    private AdvertingService advertingService;


    @Override
    public AdvertisementDto createAdvertising(Long categoryId, CreateAdvertisingDto requestDto) {
        Advertising advertising = DtoConverter.convertToAdvertising(requestDto);
        Advertising createdAdvertising = advertingService.createAdvertising(categoryId, advertising);
        return DtoConverter.convertToAdvertisingDto(createdAdvertising);
    }

    @Override
    public AdvertisementDto getAdvertising(Long categoryId, Long addId) {
        Advertising advertising = advertingService.getAdvertising(categoryId, addId);
        return DtoConverter.convertToAdvertisingDto(advertising);
    }

    @Override
    public AdvertisementDto updateAdvertising(Long categoryId, Long addId, UpdateAdvertisingDto requestDto) {
        Advertising advertising = advertingService.updateAdvertising(categoryId, addId,
                DtoConverter.convertToAdvertising(requestDto));
        return DtoConverter.convertToAdvertisingDto(advertising);
    }

    @Override
    public AdvertisementDto deleteAdvertising(Long categoryId, Long addId) {
        return DtoConverter.convertToAdvertisingDto(advertingService.deleteAdvertising(categoryId, addId));
    }

    @Override
    public ListResponse<AdvertisementDto> getAdvertisings(Long categoryId, Integer offset, Integer limit) {
        List<Advertising> allAdvertising = advertingService.getAdvertisingByCategory(categoryId);
        return ListResponse.<AdvertisementDto>builder()
                .data(DtoConverter.convertAssignmentDtoList(allAdvertising))
                .build();
    }

    @Override
    public ListResponse<AdvertisementDto> getAllAdvertisings(Integer offset, Integer limit) {
        List<Advertising> allAdvertising = advertingService.getAllAdvertising();
        return ListResponse.<AdvertisementDto>builder()
                .data(DtoConverter.convertAssignmentDtoList(allAdvertising))
                .build();
    }
}
