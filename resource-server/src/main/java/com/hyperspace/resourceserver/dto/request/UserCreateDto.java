package com.hyperspace.resourceserver.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateDto {
    private String id;
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
}
