package com.hyperspace.resourceserver.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateAdvertisingDto implements Serializable {

    private Date time;
    @NotNull(message = "title can not be null")
    @NotEmpty(message = "title can not be empty")
    private String title;
    private String imageUrl;
    @NotNull(message = "description can not be null")
    @NotEmpty(message = "description can not be empty")
    private String description;
}
