package com.hyperspace.resourceserver.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListResponse <T>{
    private int noOfElements;
    private int size;
    private long totalElements;
    private int noOfPages;
    private List<T> data;
}
