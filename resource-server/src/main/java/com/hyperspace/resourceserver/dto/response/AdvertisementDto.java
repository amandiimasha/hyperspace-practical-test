package com.hyperspace.resourceserver.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdvertisementDto {
    private long id;
    private String description;
    private String title;
    private String imageUrl;
    private Date time;
    private CategoryDto category;
}
