
package com.hyperspace.resourceserver.util;


import com.hyperspace.resourceserver.dto.request.*;
import com.hyperspace.resourceserver.dto.response.AdvertisementDto;
import com.hyperspace.resourceserver.dto.response.CategoryDto;
import com.hyperspace.resourceserver.dto.response.UserDto;
import com.hyperspace.resourceserver.model.Advertising;
import com.hyperspace.resourceserver.model.Category;
import com.hyperspace.resourceserver.model.User;
import com.hyperspace.resourceserver.util.enums.UserStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amandi Imasha
 */
public class DtoConverter {
    public static User convertToUser(UserCreateDto requestDto) {
        return User.builder()
                .email(requestDto.getEmail())
                .firstName(requestDto.getFirstName())
                .lastName(requestDto.getLastName())
                .password(requestDto.getPassword())
                .username(requestDto.getUsername())
                .status(UserStatus.ACTIVE)
                .build();
    }

    public static UserDto convertToUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .username(user.getUsername())
                .build();
    }

    public static List<UserDto> convertToDtoList(List<User> userList){
        List<UserDto> userDtos = new ArrayList<>();
        for (User u:userList) {
            userDtos.add(convertToUserDto(u));
        }
        return userDtos;
    }

    public static Advertising convertToAdvertising(CreateAdvertisingDto requestDto) {
        return Advertising.builder()
                .title(requestDto.getTitle())
                .description(requestDto.getDescription())
                .time(requestDto.getTime())
                .url(requestDto.getImageUrl())
                .build();
    }

    public static AdvertisementDto convertToAdvertisingDto(Advertising advertising) {
        return AdvertisementDto.builder()
                .id(advertising.getId())
                .title(advertising.getTitle())
                .description(advertising.getDescription())
                .time(advertising.getTime())
                .imageUrl(advertising.getUrl())
                .category(convertToCategoryDto(advertising.getCategory()))
                .build();
    }

    public static List<AdvertisementDto> convertAssignmentDtoList(List<Advertising> list){
        List<AdvertisementDto> results = new ArrayList<>();
        for (Advertising a:list) {
            results.add(convertToAdvertisingDto(a));
        }
        return results;
    }

    public static Advertising convertToAdvertising(UpdateAdvertisingDto requestDto) {
        return Advertising.builder()
                .title(requestDto.getTitle())
                .description(requestDto.getDescription())
                .time(requestDto.getTime())
                .url(requestDto.getImageUrl())
                .build();
    }

    public static CategoryDto convertToCategoryDto(Category category, List<Advertising> addList) {

        List<AdvertisementDto> advertisementDtos = convertAssignmentDtoList(addList);

        return CategoryDto.builder()
                .advertisements(advertisementDtos)
                .name(category.getName())
                .id(category.getId())
                .build();
    }

    public static CategoryDto convertToCategoryDto(Category category) {
        return CategoryDto.builder()
                .name(category.getName())
                .id(category.getId())
                .build();
    }

    public static Category convertToCategory(CategoryUpdateDto requestDto) {
        return Category.builder()
                .name(requestDto.getName())
                .build();
    }

    public static Category convertToCategory(CategoryCreateDto requestDto) {
        return Category.builder()
                .name(requestDto.getName())
                .build();
    }
}
