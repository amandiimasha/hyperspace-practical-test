/**
 * Copyright (C) 2019 Epic lanka pvt Ltd.
 * All rights reserved This software is the confidential and proprietary information of Epic lanka pvt Ltd.
 * You shall not disclose such confidential information and shall use it only in accordance with the terms
 * of the license agreement you entered into with Epic lanka pvt Ltd.
 */
package com.hyperspace.resourceserver.util.enums;

public enum FileLocationStatus {
    COMMISSIONER,
    DCOMMISSIONER,
    ACCESSOR,
    TAXOFFICER
}
