package com.hyperspace.resourceserver.util;

/**
 * @author Amandi Imasha
 */
public class PermissionCode {
    public static class PermissionPermissionCode {
        public static final String PERMISSION_ADD = "PR-0010";
        public static final String PERMISSION_READ = "PR-0011";
        public static final String PERMISSION_READ_ALL = "PR-0012";
        public static final String PERMISSION_UPDATE = "PR-0013";
    }

    public static class RolePermissionCode {
        public static final String ROLE_ADD = "PR-0020";
        public static final String ROLE_READ = "PR-0021";
        public static final String ROLE_READ_ALL = "PR-0022";
        public static final String ROLE_UPDATE = "PR-0023";
    }

    public static class UserPermissionCode {
        public static final String USER_ADD = "PR-0030";
        public static final String USER_READ = "PR-0031";
        public static final String USER_READ_ALL = "PR-0032";
        public static final String USER_UPDATE = "PR-0033";
    }
}
