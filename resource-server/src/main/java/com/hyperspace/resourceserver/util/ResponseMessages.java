/**
 * Copyright (C) 2019 Epic lanka pvt Ltd.
 * All rights reserved This software is the confidential and proprietary information of Epic lanka pvt Ltd.
 * You shall not disclose such confidential information and shall use it only in accordance with the terms
 * of the license agreement you entered into with Epic lanka pvt Ltd.
 */
package com.hyperspace.resourceserver.util;
/**
 *
 * @author Amandi Imasha
 */
public class ResponseMessages {
    //common
    public static final String ERROR_WHILE_PROCESSING = "Error while processing";
    public static final String BAD_REQUEST = "Bad request";
    public static final String SUCCESSFULLY="Request received successfully";
    //town
    public static final String TOWN_SUCCESSFULLY_SAVED = "Town successfully saved";
    public static final String TOWN_SUCCESSFULLY_DELETED = "Town successfully deleted";
    public static final String TOWN_SUCCESSFULLY_UPDATED = "Town successfully updated";
    //city
    public static final String CITY_SUCCESSFULLY_SAVED = "City successfully saved";
    public static final String CITY_SUCCESSFULLY_DELETED = "City successfully deleted";
    public static final String CITY_SUCCESSFULLY_UPDATED = "City successfully updated";
    //province
    public static final String PROVINCE_SUCCESSFULLY_SAVED = "Province successfully saved";
    public static final String PROVINCE_SUCCESSFULLY_DELETED = "Province successfully deleted";
    public static final String PROVINCE_SUCCESSFULLY_UPDATED = "Province successfully updated";
    //district
    public static final String DISTRICT_SUCCESSFULLY_SAVED = "District successfully saved";
    public static final String DISTRICT_SUCCESSFULLY_DELETED = "District successfully deleted";
    public static final String DISTRICT_SUCCESSFULLY_UPDATED = "District successfully updated";
    //user
    public static final String USER_SUCCESSFULLY_REGISTERED = "User successfully registered";
    //gramaNiladariDivision
    public static  final  String GRAMA_NILADARI_DIVISION_SAVED="Grama Niladari division successsfulley Added";
    public static  final  String GRAMA_NILADARI_DIVISION_DELETED="Grama Niladari division successsfulley deleted";
    public static  final  String GRAMA_NILADARI_DIVISION_UPDATED="Grama Niladari division successsfulley updated";
    //buildingSubType
    public static  final  String BUILDING_SUB_TYPE_SAVED="Building sub type successfully added";
    public static  final  String BUILDING_SUB_TYPE_DELETED="Building sub type successfully deleted";
    public static  final  String BUILDING_SUB_TYPE_UPDATED="Building sub type successfully updated";
    //buildingType
    public static  final  String BUILDING_TYPE_SAVED="Building type successfully added";
    public static  final  String BUILDING_TYPE_DELETED="Buildi ng type successfully deleted";
    public static  final  String BUILDING_TYPE_UPDATED="Building type successfully updated";
    //compnay categories
    public static  final  String COMPANY_CATEGORY_SAVED="Company Category successfully added";
    public static  final  String COMPANY_CATEGORY_DELETED="Company Category successfully deleted";
    public static  final  String COMPANY_CATEGORY_UPDATED="Company Category successfully updated";
    //company types
    public static  final  String COMPANY_TYPE_SAVED="company type successfully added";
    public static  final  String COMPANY_TYPE_DELETED="company type successfully deleted";
    public static  final  String COMPANY_TYPE_UPDATED="company type successfully updated";
    //deed nature
    public static  final  String DEED_NATURE_SAVED="deed nature successfully added";
    public static  final  String DEED_NATURE_DELETED="deed nature successfully deleted";
    public static  final  String DEED_NATURE_UPDATED="deed nature successfully updated";
//    //document type
//    public static  final  String DOCUMENT_TYPE_SAVED="deed nature successfully added";
//    public static  final  String DOCUMENT_TYPE_DELETED="deed nature successfully deleted";
//    public static  final  String DOCUMENT_TYPE_UPDATED="deed nature successfully updated";
    //localAuthority
    public static  final  String LOCAL_AUTHORITY_SUCCESSFULLY_SAVED="Local Authority successfully added";
    public static  final  String LOCAL_AUTHORITY_DELETED="Local Authority successfully deleted";
    public static  final  String LOCAL_AUTHORITY_UPDATED="Local Authority successfully updated";
    //office
    public static  final  String OFFICE_SUCCESSFULLY_SAVED="Office successfully added";
    public static  final  String OFFICE_DELETED="Office successfully deleted";
    public static  final  String OFFICE_UPDATED="Office successfully updated";

    //document type
    public static  final  String DOCUMENT_TYPE_SAVED="document type nature successfully added";
    public static  final  String DOCUMENT_TYPE_DELETED="document type successfully deleted";
    public static  final  String DOCUMENT_TYPE_UPDATED="document type successfully updated";
    //floor sub type
    public static  final  String FLOOR_SUB_TYPE_SAVED="floor sub type successfully added";
    public static  final  String FLOOR_SUB__TYPE_DELETED="floor sub type successfully deleted";
    public static  final  String FLOOR_SUB__TYPE_UPDATED="floor sub type successfully updated";
    //floor type
    public static  final  String FLOOR_TYPE_SAVED="floor type successfully added";
    public static  final  String FLOOR_TYPE_DELETED="floor type successfully deleted";
    public static  final  String FLOOR_TYPE_UPDATED="floor type successfully updated";
    //PaymentMethod
    public static  final  String PAYMENT_METHOD_SUCCESSFULLY_SAVED="Payment successfully added";
    public static  final  String PAYMENT_METHOD_DELETED="Payment successfully deleted";
    public static  final  String PAYMENT_METHOD_UPDATED="Payment successfully updated";
    //Payment
    public static  final  String PAYMENT_SUCCESSFULLY_SAVED="Payment successfully added";
    public static  final  String PAYMENT_DELETED="Payment successfully deleted";
    public static  final  String PAYMENT_UPDATED="Payment successfully updated";
    public static  final  String PAYMENT_SUCCESS="Payment successfully";
    //PropertyFacility
    public static  final  String PROPERTY_FACILITY_SUCCESSFULLY_SAVED="Property Facility successfully added";
    public static  final  String PROPERTY_FACILITY_DELETED="Property Facility successfully deleted";
    public static  final  String PROPERTY_FACILITY_UPDATED="Property Facility successfully updated";
    //PropertyType
    public static  final  String PROPERTY_TYPE_SUCCESSFULLY_SAVED="Property Type successfully added";
    public static  final  String PROPERTY_TYPE_DELETED="Property Type successfully deleted";
    public static  final  String PROPERTY_TYPE_UPDATED="Property Type successfully updated";
    //PropertyUsage
    public static  final  String PROPERTY_USAGE_SUCCESSFULLY_SAVED="Property Usage successfully added";
    public static  final  String PROPERTY_USAGE_DELETED="Property Usage successfully deleted";
    public static  final  String PROPERTY_USAGE_UPDATED="Property Usage successfully updated";
    //PropertyUsageSubType
    public static  final  String PROPERTY_USAGE_SUBTYPE_SUCCESSFULLY_SAVED="Property Usage SubType successfully added";
    public static  final  String PROPERTY_USAGE_SUBTYPE_DELETED="Property Usage SubType successfully deleted";
    public static  final  String PROPERTY_USAGE_SUBTYPE_UPDATED="Property Usage SubType successfully updated";
    //ValuationRequest
    public static  final  String VALUATION_REQUEST_SUCCESSFULLY_SAVED="Valuation Request successfully added";
    public static  final  String VALUATION_REQUEST_DELETED="Valuation Request successfully deleted";
    public static  final  String VALUATION_REQUEST_UPDATED="Valuation Request successfully updated";
    public static  final  String VALUATION_REQUEST_SAVING_FAILED="Valuation Request successfully updated";
}
