package com.hyperspace.resourceserver.repository;

import com.hyperspace.resourceserver.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Amandi Imasha
 */
@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long>,
        QuerydslPredicateExecutor<Permission> {
}
