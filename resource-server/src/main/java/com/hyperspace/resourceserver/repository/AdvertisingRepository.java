package com.hyperspace.resourceserver.repository;

import com.hyperspace.resourceserver.model.Advertising;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Amandi Imasha
 */
@Repository
public interface AdvertisingRepository extends JpaRepository<Advertising,Long>,
        QuerydslPredicateExecutor<Advertising> {
}
