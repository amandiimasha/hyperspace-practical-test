package com.hyperspace.resourceserver.service.custom.impl;

import com.hyperspace.resourceserver.exception.SystemException;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.util.JsonSerialization;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
public class KeyClockDataService {

    private Keycloak keycloak;

    @Value("${keyclock-client-config.target-realm}")
    private String TARGET_REALM;
    @Value("${keyclock-client-config.server-url}")
    private String SERVER_URL;
    @Value("${keyclock-client-config.realm}")
    private String MASTER_REALM;
    @Value("${keyclock-client-config.master-user.username}")
    private String MASTER_USERNAME;
    @Value("${keyclock-client-config.master-user.password}")
    private String MASTER_PASSWORD;
    @Value("${keyclock-client-config.client-id}")
    private String MASTER_CLIENT_ID;
    @Value("${keyclock-client-config.client-secret}")
    private String MASTER_CLIENT_SECRET;

    @PostConstruct
    private void init(){
        this.keycloak = KeycloakBuilder.builder()
                .serverUrl(SERVER_URL)
                .realm(MASTER_REALM)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(MASTER_CLIENT_ID)
                .clientSecret(MASTER_CLIENT_SECRET)
                .username(MASTER_USERNAME)
                .password(MASTER_PASSWORD)
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                .build();
    }

    @Scheduled(fixedRate = 60000)
    public void hb() {
        try {
            log.info("keycloak server hb start");
            List<UserRepresentation> users = getUsers();
            if (null != users) {
                log.info("keycloak connection is alive");
            } else {
                log.warn("keycloak connection is dead");
            }
            log.info("keycloak server hb finished");
        } catch (Exception e) {
            log.error("keycloak hb error ", e);
        }

    }

    public RealmResource getRealmResource() {
        return keycloak.realms().realm(TARGET_REALM);
    }
    private void handleClientErrorException(ClientErrorException e) throws SystemException {
        Response response = e.getResponse();
        //
        try {
            Map error = JsonSerialization.readValue((ByteArrayInputStream) response.getEntity(), Map.class);
            log.error("keycloak error {}", e.getMessage(), e);
            throw new SystemException("reason: " + response.getStatusInfo().getReasonPhrase() + "\n" + "error_description: " + error.get("error_description"));
        } catch (IOException ex) {
            log.error("keycloak error {}", ex.getMessage(), e);
        }
    }

    public UserRepresentation findUserByUserName(String userName) {
        UserRepresentation userRepresentation = null;
        List<UserRepresentation> list = getRealmResource().users().search(userName);
        int count = 0;
        for (UserRepresentation ur : list) {
            if (ur.getUsername().equalsIgnoreCase(userName)) {
                userRepresentation = ur;
                break;
            }
        }
        return userRepresentation;
    }

    public RoleRepresentation createRole(String roleName) {
        RoleRepresentation savedRoleRepresentation = null;
        try {
            RealmResource realmResource = getRealmResource();
            //check duplicate role name
            boolean hasRole = false;
            List<RoleRepresentation> list = realmResource.roles().list();
            for (RoleRepresentation roleRep : list) {
                if (roleRep.getName().equalsIgnoreCase(roleName)) {
                    savedRoleRepresentation = roleRep;
                    hasRole = true;
                    break;
                }
            }
            if (!hasRole) {
                RoleRepresentation representation = new RoleRepresentation();
                representation.setName(roleName);
                representation.setId(UUID.randomUUID().toString());
                //
                realmResource.roles().create(representation);

                return representation;
            } else {
                log.info("keycloak role already saved {}", roleName);
                return savedRoleRepresentation;
            }

        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
                return null;
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
    }

    public UserRepresentation createUser(String userName, String firstName, String lastName, String email, String generatedPassword) {
        log.info("keycloak user create request {} ", userName);
        try {
            if (null == findUserByUserName(userName)) {
                //
                CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
                credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
                credentialRepresentation.setValue(generatedPassword);
                credentialRepresentation.setTemporary(false);
                //
                UserRepresentation userRepresentation = new UserRepresentation();
                userRepresentation.setUsername(userName);
                userRepresentation.setFirstName(firstName);
                userRepresentation.setLastName(lastName);
                userRepresentation.setEmail(email);
                userRepresentation.setEnabled(false);
                userRepresentation.setCredentials(Arrays.asList(credentialRepresentation));
                //
                RealmResource realmResource = getRealmResource();
                Response response = realmResource.users().create(userRepresentation);
                if (response.getStatus() == 201) {
                    log.info("keycloak user created ", userName);
                    //find by user name
                    UserRepresentation findUserByUserName = findUserByUserName(userName);
                    return findUserByUserName;
                } else {
                    log.error("keycloak user create response status {} {}", userName, response.getStatus());
                    return null;
                }
            } else {
                log.warn("keycloak user already created {}", userName);
                return findUserByUserName(userName);
            }
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
                return null;
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
    }

    public void userRoleAssign(String userName, String roleName) {
        try {
            RealmResource realmResource = getRealmResource();
            UserRepresentation userRepresentation = findUserByUserName(userName);
            if (null != userRepresentation) {
                RoleRepresentation roleRepresentation = realmResource.roles().get(roleName).toRepresentation();
                if (null != roleRepresentation) {
                    realmResource.users().get(userRepresentation.getId()).roles().realmLevel().add(Arrays.asList(roleRepresentation));
                    log.info("keycloak user role assigned {} {}", userName, roleName);
                }
            }
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
    }

    public void userRoleUnAssign(String userName, String roleName) {
        try {
            RealmResource realmResource = getRealmResource();
            UserRepresentation userRepresentation = findUserByUserName(userName);
            if (null != userRepresentation) {
                RoleRepresentation roleRepresentation = realmResource.roles().get(roleName).toRepresentation();
                if (null != roleRepresentation) {
                    realmResource.users().get(userRepresentation.getId()).roles().realmLevel().remove(Arrays.asList(roleRepresentation));
                    log.info("keycloak user role unassigned {} {}", userName, roleName);
                }
            }
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
    }

    public void userStatusChange(String userName, boolean status) {
        try {
            RealmResource realmResource = getRealmResource();
            UserRepresentation userRepresentation = findUserByUserName(userName);
            if (null != userRepresentation) {
                userRepresentation.setEnabled(status);
                realmResource.users().get(userRepresentation.getId()).update(userRepresentation);
                log.info("keycloak user status changed  {} {}", userName, status);
            } else {
                log.error("username {} not found in keycloak", userName);
            }
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }

    }

    public void updateUserDeatil(String userName, String firstName, String lastName) {
        try {
            RealmResource realmResource = getRealmResource();
            UserRepresentation userRepresentation = findUserByUserName(userName);
            if (null != userRepresentation) {
                userRepresentation.setFirstName(firstName);
                userRepresentation.setLastName(lastName);
                realmResource.users().get(userRepresentation.getId()).update(userRepresentation);
                log.info("keycloak user updated  {} ", userName);
            }
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
    }

    public void userPasswordChange(String userName, String newPassword) {
        try {
            RealmResource realmResource = getRealmResource();
            UserRepresentation userRepresentation = findUserByUserName(userName);
            if (null != userRepresentation) {
                CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
                credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
                credentialRepresentation.setValue(newPassword);
                credentialRepresentation.setTemporary(false);
                //
                realmResource.users().get(userRepresentation.getId()).resetPassword(credentialRepresentation);
                log.info("keycloak user password chnaged  {} ", userName);
            }
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
    }

    public List<UserRepresentation> getUsers() {
        try {
            RealmResource realmResource = getRealmResource();
            List<UserRepresentation> list = realmResource.users().list();
            return list;
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
        return null;
    }

    public boolean oldPasswordCheck(String userName, String password) {
        try {
            log.info("password checking for {} ", userName);
            UserRepresentation userRepresentation = findUserByUserName(userName);
            log.info("keycloak user found for username {} kcloak id {}", userName, userRepresentation.getId());
            List<CredentialRepresentation> credentials = getRealmResource().users().get(userRepresentation.getId()).credentials();
            for (CredentialRepresentation credential : credentials) {
                if (null != credential) {
                    System.out.println(credential.toString());
                    if (credential.getType().equals(CredentialRepresentation.PASSWORD)) {
                        if (credential.getValue().equals(password)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
        return false;
    }

    public ClientRepresentation createClient(String clientId) {
        try {
            ClientRepresentation clientRepresentation = new ClientRepresentation();
            clientRepresentation.setClientId(clientId);
            clientRepresentation.setClientAuthenticatorType("bearer-only");
            //
            getRealmResource().clients().create(clientRepresentation);
            //
            ClientRepresentation savedClientRepresentation = getRealmResource().clients().findByClientId(clientId).get(0);
            return savedClientRepresentation;
        } catch (Exception e) {
            if (e instanceof ClientErrorException) {
                handleClientErrorException((ClientErrorException) e);
            } else {
                log.error("keycloak error {}", e.getMessage(), e);
                throw new SystemException(e.getMessage());
            }
        }
        return null;
    }


}
