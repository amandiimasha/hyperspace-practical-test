/**
 * Copyright (C) 2019 Epic lanka pvt Ltd.
 * All rights reserved This software is the confidential and proprietary information of Epic lanka pvt Ltd.
 * You shall not disclose such confidential information and shall use it only in accordance with the terms
 * of the license agreement you entered into with Epic lanka pvt Ltd.
 */
package com.hyperspace.resourceserver.service.custom;


import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.model.User;
import com.hyperspace.resourceserver.service.SuperService;

import java.util.List;

/**
 *
 * @author Amandi Imasha
 */
public interface UserService extends SuperService<User,String> {

    public User findByUserName(String name);
    public User findByUserNameAndEmail(String name, String email) throws RecordNotFoundException;
}
