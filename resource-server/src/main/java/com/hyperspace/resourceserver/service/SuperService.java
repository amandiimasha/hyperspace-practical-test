package com.hyperspace.resourceserver.service;

import com.hyperspace.resourceserver.exception.DuplicateRecordException;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author Amandi Imasha
 */
public interface SuperService <T,ID>{
    public T save(T t)throws DuplicateRecordException;
    public Optional<T> findById(ID id)throws RecordNotFoundException;
    public List<T> getAll();
    public Page<T> getPaginatedList(Integer offset, Integer limit);
    public T delete(ID id)throws RecordNotFoundException;
    public T update(ID id,T t)throws RecordNotFoundException;
}
