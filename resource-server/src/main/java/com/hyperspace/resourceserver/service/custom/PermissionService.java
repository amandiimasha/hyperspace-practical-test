package com.hyperspace.resourceserver.service.custom;

import com.hyperspace.resourceserver.model.Permission;
import com.hyperspace.resourceserver.service.SuperService;

/**
 * @author Amandi Imasha
 */
public interface PermissionService extends SuperService<Permission, Long> {
    Permission findByCode(String code);

}
