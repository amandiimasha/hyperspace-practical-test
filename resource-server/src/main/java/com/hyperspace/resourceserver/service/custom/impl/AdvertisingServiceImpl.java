package com.hyperspace.resourceserver.service.custom.impl;

import com.google.common.collect.Lists;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.model.Advertising;
import com.hyperspace.resourceserver.model.Category;
import com.hyperspace.resourceserver.model.QAdvertising;
import com.hyperspace.resourceserver.repository.AdvertisingRepository;
import com.hyperspace.resourceserver.service.custom.AdvertingService;
import com.hyperspace.resourceserver.service.custom.CategoryService;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class AdvertisingServiceImpl implements AdvertingService {

    @Autowired
    private AdvertisingRepository advertisingRepository;
    @Autowired
    private CategoryService categoryService;

    @Override
    public Advertising createAdvertising(Long categoryId, Advertising advertising) {
        Optional<Category> found = categoryService.findById(categoryId);
        if (found.isPresent()) {
            advertising.setCategory(found.get());
            return advertisingRepository.save(advertising);
        } else {
            throw new RecordNotFoundException("can not found category by given id : " + categoryId);
        }
    }

    @Override
    public Advertising updateAdvertising(Long categoryId, Long addId, Advertising advertising) {
        if (null != getAdvertising(categoryId,addId)) {
            advertising.setCategory(categoryService.findById(categoryId).get());
            advertising.setId(addId);
            Advertising save = advertisingRepository.save(advertising);
            log.info("advertisement updated {}", save);
            return save;
        } else {
            throw new RecordNotFoundException("can not found category by given id : " + categoryId);

        }
    }

    @Override
    public Advertising deleteAdvertising(Long categoryId, Long id) {
        Optional<Category> found = categoryService.findById(categoryId);
        if (found.isPresent()) {
            Optional<Advertising> foundAdd = advertisingRepository.findById(id);
            if (foundAdd.isPresent()) {
                advertisingRepository.delete(foundAdd.get());
                log.info("advertisement deleted {}", foundAdd.get());
                return foundAdd.get();
            } else {
                throw new RecordNotFoundException("can not found advertisement for given id :" + id);
            }
        } else {
            throw new RecordNotFoundException("can not found category by given id : " + categoryId);

        }
    }

    @Override
    public Advertising getAdvertising(Long categoryId, Long id) {
        if (categoryService.findById(categoryId).isPresent()){
            QAdvertising qAdvertising = QAdvertising.advertising;
            BooleanExpression and = qAdvertising.category.id.eq(categoryId)
                    .and(qAdvertising.id.eq(id));
            Optional<Advertising> one = advertisingRepository.findOne(and);
            if (one.isPresent()){
                return one.get();
            } else {
                throw new RecordNotFoundException("can not found");
            }
        } else {
            throw new RecordNotFoundException("can not found category by given id :"+categoryId);
        }
    }

    @Override
    public List<Advertising> getAdvertisingByCategory(Long categoryId) {
        Optional<Category> found = categoryService.findById(categoryId);
        if (found.isPresent()) {
            QAdvertising qAdvertising = QAdvertising.advertising;
            BooleanExpression expression = qAdvertising.category.id.eq(categoryId);
            Iterable<Advertising> all = advertisingRepository.findAll(expression);
            log.info("list {}", Lists.newArrayList(all));
            return Lists.newArrayList(all);
        } else {
            throw new RecordNotFoundException("can not found category by given id : " + categoryId);
        }
    }

    @Override
    public List<Advertising> getAllAdvertising() {
        return advertisingRepository.findAll();
    }

    @Override
    public Page<Advertising> getAllAdvertising(int offset, int limit) {
        PageRequest of = PageRequest.of(offset, limit);
        return advertisingRepository.findAll(of);
    }
}
