package com.hyperspace.resourceserver.service.custom;

import com.hyperspace.resourceserver.model.Advertising;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AdvertingService {

    public Advertising createAdvertising(Long categoryId,Advertising advertising);
    public Advertising updateAdvertising(Long categoryId,Long addId, Advertising advertising);
    public Advertising deleteAdvertising(Long categoryId,Long id);
    public Advertising getAdvertising(Long categoryId,Long id);
    public List<Advertising> getAdvertisingByCategory(Long categoryId);
    public List<Advertising> getAllAdvertising();
    public Page<Advertising> getAllAdvertising(int offset, int limit);
}
