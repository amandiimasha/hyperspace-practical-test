package com.hyperspace.resourceserver.service.custom;

import com.hyperspace.resourceserver.model.Role;
import com.hyperspace.resourceserver.service.SuperService;

/**
 * @author Amandi Imasha
 */
public interface RoleService extends SuperService<Role, Long> {
    public Role findByCode(String code);
}
