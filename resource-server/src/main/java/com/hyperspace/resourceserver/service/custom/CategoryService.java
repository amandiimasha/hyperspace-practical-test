package com.hyperspace.resourceserver.service.custom;

import com.hyperspace.resourceserver.model.Category;
import com.hyperspace.resourceserver.service.SuperService;

public interface CategoryService extends SuperService<Category,Long> {
    public Category findByName(String name);
}
