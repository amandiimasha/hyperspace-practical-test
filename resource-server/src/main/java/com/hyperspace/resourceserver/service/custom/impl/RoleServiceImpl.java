package com.hyperspace.resourceserver.service.custom.impl;

import com.hyperspace.resourceserver.exception.DuplicateRecordException;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.model.QRole;
import com.hyperspace.resourceserver.model.Role;
import com.hyperspace.resourceserver.repository.RoleRepository;
import com.hyperspace.resourceserver.service.custom.RoleService;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role save(Role role) throws DuplicateRecordException {
        if(null == findByCode(role.getCode())){
            return roleRepository.save(role);
        } else {
            throw new DuplicateRecordException("role already exist "+role.getCode());
        }
    }

    @Override
    public Optional<Role> findById(Long id) throws RecordNotFoundException {
        return roleRepository.findById(id);
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    @Override
    public Page<Role> getPaginatedList(Integer offset, Integer limit) {
        PageRequest request = PageRequest.of(offset, limit);
        return roleRepository.findAll(request);
    }

    @Override
    public Role delete(Long aLong) throws RecordNotFoundException {
        return null;
    }

    @Override
    public Role update(Long aLong, Role role) throws RecordNotFoundException {
        return null;
    }

    @Override
    public Role findByCode(String code) {
        QRole qRole = QRole.role;
        BooleanExpression expression = qRole.code.eq(code);
        return roleRepository.findOne(expression).orElse(null);
    }
}
