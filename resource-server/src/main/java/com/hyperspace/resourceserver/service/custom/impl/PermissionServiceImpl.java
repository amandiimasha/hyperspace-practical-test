package com.hyperspace.resourceserver.service.custom.impl;

import com.hyperspace.resourceserver.exception.DuplicateRecordException;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.model.Permission;
import com.hyperspace.resourceserver.model.QPermission;
import com.hyperspace.resourceserver.repository.PermissionRepository;
import com.hyperspace.resourceserver.service.custom.PermissionService;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;


    @Override
    public Permission save(Permission permission) throws DuplicateRecordException {
        if (null == findByCode(permission.getCode())){
            Permission save = permissionRepository.save(permission);
            log.info("permission saved {}",permission);
            return save;
        } else {
            throw new DuplicateRecordException("permission already exist "+permission.getCode());
        }
    }

    @Override
    public Optional<Permission> findById(Long id) throws RecordNotFoundException {
        return permissionRepository.findById(id);
    }

    @Override
    public List<Permission> getAll() {
        return permissionRepository.findAll();
    }

    @Override
    public Page<Permission> getPaginatedList(Integer offset, Integer limit) {
        PageRequest request = PageRequest.of(offset, limit);
        return permissionRepository.findAll(request);
    }

    @Override
    public Permission delete(Long id) throws RecordNotFoundException {
        if (findById(id).isPresent()){
            Permission permission = findById(id).get();
            permissionRepository.delete(permission);
            log.info("permission has been deleted {}",permission);
            return permission;
        } else {
            throw new RecordNotFoundException("can not found permission by given id "+ id);
        }
    }

    @Override
    public Permission update(Long aLong, Permission permission) throws RecordNotFoundException {
        return null;
    }

    @Override
    public Permission findByCode(String code) {
        QPermission qPermission = QPermission.permission;
        BooleanExpression expression = qPermission.code.eq(code);
        return permissionRepository.findOne(expression).orElse(null);
    }
}
