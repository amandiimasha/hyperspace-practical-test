package com.hyperspace.resourceserver.service.custom.impl;

import com.hyperspace.resourceserver.exception.DuplicateRecordException;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.model.QUser;
import com.hyperspace.resourceserver.model.User;
import com.hyperspace.resourceserver.repository.UserRepository;
import com.hyperspace.resourceserver.service.custom.UserService;
import com.hyperspace.resourceserver.util.enums.UserStatus;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Amandi Imasha
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private KeyClockDataService keyClockDataService;

    @Override
    public User save(User user) throws DuplicateRecordException {
        if (null == findByUserName(user.getUsername())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            try {
                keyClockDataService.createUser(user.getUsername(),
                        user.getFirstName(),
                        user.getLastName(),
                        user.getEmail(),
                        user.getPassword());
            } catch (Exception e) {
                log.warn("keycloak user create error");
            }
            return userRepository.save(user);
        } else {
            throw new DuplicateRecordException("user name already exist");
        }
    }

    @Override
    public Optional<User> findById(String id) throws RecordNotFoundException {
        return userRepository.findById(id);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public Page<User> getPaginatedList(Integer offset, Integer limit) {
        PageRequest request = PageRequest.of(offset, limit);
        return userRepository.findAll(request);
    }

    @Override
    public User delete(String id) throws RecordNotFoundException {
        Optional<User> optionalUser = findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            user.setStatus(UserStatus.DELETED);
            userRepository.save(user);
            log.info("user has been deleted {}",user);
            return user;
        } else {
            throw new RecordNotFoundException("can not found user by given id : "+id);
        }
    }

    @Override
    public User update(String id, User user) throws RecordNotFoundException {
        Optional<User> optionalUser = findById(id);
        if(optionalUser.isPresent()){
            user.setId(user.getId());
            userRepository.save(user);
            log.info("user has been updated {}",user);
            return user;
        } else {
            throw new RecordNotFoundException("can not found user by given id : "+id);
        }
    }

    @Override
    public User findByUserName(String name) {
        QUser qUser = QUser.user;
        BooleanExpression expression = qUser.username.eq(name);
        return userRepository.findOne(expression).orElse(null);
    }

    @Override
    public User findByUserNameAndEmail(String name, String email) throws RecordNotFoundException {
        QUser qUser = QUser.user;
        BooleanExpression expression = qUser.username.eq(name)
                .and(qUser.email.eq(email));
        return userRepository.findOne(expression).orElse(null);
    }
}
