package com.hyperspace.resourceserver.service.custom.impl;

import com.hyperspace.resourceserver.exception.DuplicateRecordException;
import com.hyperspace.resourceserver.exception.RecordNotFoundException;
import com.hyperspace.resourceserver.model.Advertising;
import com.hyperspace.resourceserver.model.Category;
import com.hyperspace.resourceserver.model.QCategory;
import com.hyperspace.resourceserver.repository.CategoryRepository;
import com.hyperspace.resourceserver.service.custom.AdvertingService;
import com.hyperspace.resourceserver.service.custom.CategoryService;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private AdvertingService advertingService;

    @Override
    public Category save(Category category) throws DuplicateRecordException {
        if (null == findByName(category.getName())){
            Category save = categoryRepository.save(category);
            log.info("category saved {}", category);
            return save;
        } else {
            throw new DuplicateRecordException("category already exist");
        }
    }

    @Override
    public Optional<Category> findById(Long id) throws RecordNotFoundException {
        return categoryRepository.findById(id);
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Page<Category> getPaginatedList(Integer offset, Integer limit) {
        PageRequest of = PageRequest.of(offset, limit);
        return categoryRepository.findAll(of);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Category delete(Long id) throws RecordNotFoundException {
        Optional<Category> found = findById(id);
        if (found.isPresent()){
            List<Advertising> addList = advertingService.getAdvertisingByCategory(id);
            for (Advertising a:addList) {
                advertingService.deleteAdvertising(id,a.getId());
            }
            categoryRepository.delete(found.get());
            log.info("category deleted {}", found.get());
            return found.get();
        } else {
            throw new RecordNotFoundException("can not found category for given id :"+ id);
        }
    }

    @Override
    public Category update(Long id, Category category) throws RecordNotFoundException {
        return null;
    }

    @Override
    public Category findByName(String name) {
        QCategory qCategory = QCategory.category;
        BooleanExpression expression = qCategory.name.eq(name);
        return categoryRepository.findOne(expression).orElse(null);
    }
}
