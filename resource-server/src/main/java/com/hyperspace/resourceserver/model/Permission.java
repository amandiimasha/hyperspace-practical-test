package com.hyperspace.resourceserver.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Amandi Imasha
 */
@Entity
@Table(name = "permission")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Permission extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "code", unique = true)
    private String code;

    @Version
    @Column(name = "version")
    private int version;

}
