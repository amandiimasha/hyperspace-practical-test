package com.hyperspace.resourceserver.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Amandi Imasha
 */
@Entity
@Table(name = "advertising")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Advertising extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title")
    private String title;
    @Column(name = "url")
    @Lob
    private String url;
    @Column(name = "description")
    private String description;
    @Column(name = "time")
    private Date time;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

}
