package com.hyperspace.resourceserver.model;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 *
 * @author Amandi Imasha
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {

    @CreatedBy
    protected U created_by;

    @CreatedDate
    @Temporal(TIMESTAMP)
    protected Date created_date;

    @LastModifiedBy
    protected U last_modified_by;

    @LastModifiedDate
    @Temporal(TIMESTAMP)
    protected Date last_modified_date;
}
