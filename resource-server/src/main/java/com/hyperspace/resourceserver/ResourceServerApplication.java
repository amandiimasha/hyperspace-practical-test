package com.hyperspace.resourceserver;

import com.hyperspace.resourceserver.audit.AuditorAwareImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class ResourceServerApplication {

	public static final String API_NAME = "HYPERSPACE REST API";
	public static final String API_VERSION = "0.0.1";
	public static final String BASE_CONTROLLER_PACKAGE = "com.hyperspace";

	public static void main(String[] args) {
		SpringApplication.run(ResourceServerApplication.class, args);
	}

	@Bean
	public AuditorAware<String> auditorAware() {
		return new AuditorAwareImpl();
	}

}
