package com.hyperspace.resourceserver.exception;

/**
 * @author Amandi Imasha
 */
public class SystemException extends RuntimeException {

    private String errorCode = "HYPERSPACE-0000";


    public SystemException(String message) {
        super(message);
        this.errorCode = "HYPERSPACE-0000";
    }

    public SystemException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }


}
