package com.hyperspace.resourceserver.exception;
/**
 *
 * @author Amandi Imasha
 */
public class RecordNotFoundException extends SystemException {

    public static final String ERROR_CODE = "HYPERSPACE-1000";

    public RecordNotFoundException(String message) {
        super(ERROR_CODE, message);
    }

}
