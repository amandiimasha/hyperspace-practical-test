package com.hyperspace.resourceserver.exception;
/**
 *
 * @author Amandi Imasha
 */
public class DuplicateRecordException extends SystemException{

    public static final String ERROR_CODE="HYPERSPACE-2000";

    public DuplicateRecordException(String message) {
        super(ERROR_CODE,message);
    }

}
