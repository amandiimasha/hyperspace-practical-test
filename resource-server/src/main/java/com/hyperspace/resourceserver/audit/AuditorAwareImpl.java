package com.hyperspace.resourceserver.audit;

import com.hyperspace.resourceserver.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 *
 * @author Amandi Imasha
 */
@Slf4j
public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(SecurityUtil.getLoggedUsername());

    }
}
