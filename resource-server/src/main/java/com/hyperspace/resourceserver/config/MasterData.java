/**
 * Copyright (C) 2019 Epic lanka pvt Ltd.
 * All rights reserved This software is the confidential and proprietary information of Epic lanka pvt Ltd.
 * You shall not disclose such confidential information and shall use it only in accordance with the terms
 * of the license agreement you entered into with Epic lanka pvt Ltd.
 */
package com.hyperspace.resourceserver.config;


import com.hyperspace.resourceserver.exception.DuplicateRecordException;
import com.hyperspace.resourceserver.model.Category;
import com.hyperspace.resourceserver.model.Permission;
import com.hyperspace.resourceserver.model.Role;
import com.hyperspace.resourceserver.model.User;
import com.hyperspace.resourceserver.service.custom.CategoryService;
import com.hyperspace.resourceserver.service.custom.PermissionService;
import com.hyperspace.resourceserver.service.custom.RoleService;
import com.hyperspace.resourceserver.service.custom.UserService;
import com.hyperspace.resourceserver.util.PermissionCode;
import com.hyperspace.resourceserver.util.enums.DataStatus;
import com.hyperspace.resourceserver.util.enums.UserStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Amandi Imasha
 */
@Component
@Slf4j
public class MasterData {

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryService categoryService;


    @PostConstruct
    public void appReady() {
        log.info("app is ready checking and adding master data");
        addAndCheckPermissions();
        addAndCheckRoles();
        addDefaultUsers();
        addDefaultCategories();
    }

    private void addDefaultCategories() {
        Category news = Category.builder()
                .name("News")
                .build();


        Category sport = Category.builder()
                .name("Sport")
                .build();

        saveCategory(news);
        saveCategory(sport);
    }

    private void saveCategory(Category category){
        try {
            categoryService.save(category);
        }catch (DuplicateRecordException e){

        }
    }

    private void addAndCheckPermissions() {
        //Permission adding
        savePermission("Permission create", PermissionCode.PermissionPermissionCode.PERMISSION_ADD);
        //Permission update
        savePermission("Permission update", PermissionCode.PermissionPermissionCode.PERMISSION_UPDATE);
        //read all permission
        savePermission("Read All Permission", PermissionCode.PermissionPermissionCode.PERMISSION_READ_ALL);
        //search one permission by id
        savePermission("Search Permission", PermissionCode.PermissionPermissionCode.PERMISSION_READ);
        //add role
        savePermission("Role Create", PermissionCode.RolePermissionCode.ROLE_ADD);
        //search role
        savePermission("Get Role", PermissionCode.RolePermissionCode.ROLE_READ);
        //get all role
        savePermission("Get all role", PermissionCode.RolePermissionCode.ROLE_READ_ALL);
        //update role
        savePermission("Update Role", PermissionCode.RolePermissionCode.ROLE_UPDATE);
    }

    private void savePermission(String permissionName, String permissionCode) {
        try {// permission add
            permissionService.save(Permission.builder()
                    .code(permissionCode)
                    .name(permissionName)
                    .build());
        } catch (DuplicateRecordException e) {
            log.info("permission " + permissionCode + " already added");
        }
    }

    private void addAndCheckRoles() {
        try {// user registration
            Role role = roleService.findByCode("RO-0001");
            if (null == role) {
                //get permissions for registration role
                List<Permission> permissions = new ArrayList<>();
                //
                Permission p1 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_ADD);
                permissions.add(p1);
                Permission p2 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_UPDATE);
                permissions.add(p2);
                Permission p3 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_READ);
                permissions.add(p3);
                Permission p4 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_READ_ALL);
                permissions.add(p4);
                Permission p5 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_ADD);
                permissions.add(p5);
                Permission p6 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_UPDATE);
                permissions.add(p6);
                Permission p7 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_READ);
                permissions.add(p7);
                Permission p8 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_READ_ALL);
                permissions.add(p8);

                roleService.save(Role.builder()
                        .code("RO-0001")
                        .name("Super User")
                        .permissions(permissions)
                        .status(DataStatus.ACTIVE)
                        .build());

                log.info("added role RO-0001");
            }
        } catch (DuplicateRecordException e) {
            log.info("role RO-0001 already added");
        }

        try {// user registration
            Role role = roleService.findByCode("RO-0002");
            if (null == role) {
                //get permissions for registration role
                List<Permission> permissions = new ArrayList<>();
                //
                Permission p1 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_ADD);
                permissions.add(p1);
                Permission p2 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_UPDATE);
                permissions.add(p2);
                Permission p3 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_READ);
                permissions.add(p3);
                Permission p4 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_READ_ALL);
                permissions.add(p4);
                Permission p5 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_ADD);
                permissions.add(p5);
                Permission p6 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_UPDATE);
                permissions.add(p6);
                Permission p7 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_READ);
                permissions.add(p7);
                Permission p8 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_READ_ALL);
                permissions.add(p8);


                roleService.save(Role.builder()
                        .code("RO-0002")
                        .name("Internal User")
                        .permissions(permissions)
                        .status(DataStatus.ACTIVE)
                        .build());
                log.info("added role RO-0002");
            }
        } catch (DuplicateRecordException e) {
            log.info("role RO-0002 already added");
        }

        try {// user registration
            Role role = roleService.findByCode("RO-0003");
            if (null == role) {
                List<Permission> permissions = new ArrayList<>();
                //
                Permission p1 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_ADD);
                permissions.add(p1);
                Permission p2 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_UPDATE);
                permissions.add(p2);
                Permission p3 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_READ);
                permissions.add(p3);
                Permission p4 = permissionService.findByCode(PermissionCode.PermissionPermissionCode.PERMISSION_READ_ALL);
                permissions.add(p4);
                Permission p5 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_ADD);
                permissions.add(p5);
                Permission p6 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_UPDATE);
                permissions.add(p6);
                Permission p7 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_READ);
                permissions.add(p7);
                Permission p8 = permissionService.findByCode(PermissionCode.RolePermissionCode.ROLE_READ_ALL);
                permissions.add(p8);


                roleService.save(Role.builder()
                        .code("RO-0003")
                        .name("Notary")
                        .permissions(permissions)
                        .status(DataStatus.ACTIVE)
                        .build());
                log.info("added role RO-0003");
            }
        } catch (DuplicateRecordException e) {
            log.info("role RO-0003 already added");
        }
    }

    private void addDefaultUsers() {
        try {
            User superuser = userService.findByUserName("superuser");
            if (null == superuser) {
                User user = new User();
                user.setUsername("superuser");
                user.setEmail("superuser@hyperspace.com");
                user.setFirstName("superuser");
                user.setLastName("superuser");
                user.setPassword("123456");
                //get all roles
                Role role = roleService.findByCode("RO-0001");
                List<Role> roles = new ArrayList<>();
                roles.add(role);
                user.setRoles(roles);
                //user.setUserType(UserType.ADMIN);
                user.setStatus(UserStatus.ACTIVE);
                //
                userService.save(user);
                log.info("superuser user saved");
            }
        } catch (DuplicateRecordException e) {
            log.info("superuser user  already added");
        }

        try {
            User systemuser = userService.findByUserName("systemuser");
            if (null == systemuser) {
                User user = new User();
                user.setUsername("systemuser");
                user.setEmail("engineering@hyperspace.com");
                user.setFirstName("System");
                user.setLastName("User");
                user.setPassword("123456");
                //get all roles
                Role role = roleService.findByCode("RO-0003");
                List<Role> roles = new ArrayList<>();
                roles.add(role);
                user.setRoles(roles);
                //user.setUserType(UserType.ADMIN);
                user.setStatus(UserStatus.ACTIVE);
                //
                userService.save(user);
                log.info("system user saved");
            }
        } catch (DuplicateRecordException e) {
            log.info("system user  already added");
        }
    }
}
