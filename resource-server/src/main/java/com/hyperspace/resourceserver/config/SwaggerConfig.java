package com.hyperspace.resourceserver.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amandi Imasha
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI(@Value("${springdoc.version}") String appVersion,
                                 @Value("${springdoc.security.authorization-url}") String authUrl,
                                 @Value("${springdoc.security.token-url}") String tokenUrl,
                                 @Value("${springdoc.api-url}") String apiUrl) {

        return new OpenAPI()
                .servers(getServers(apiUrl))
                .components(new Components()
                        .addSecuritySchemes("keycloakAuth",
                                new SecurityScheme()
                                        .type(SecurityScheme.Type.OAUTH2)
                                        .in(SecurityScheme.In.HEADER)
                                        .bearerFormat("jwt")
                                        .flows(new OAuthFlows()
                                                .password(new OAuthFlow()
                                                        .authorizationUrl(authUrl)
                                                        .tokenUrl(tokenUrl)))))
                .info(new Info()
                        .title("Hyperspace API")
                        .version(appVersion)
                        .contact(new Contact().email("engineering@hyperspace.com")));
    }

    private List<Server> getServers(String apiUrl) {
        Server defaultServer = new Server();
        defaultServer.setUrl(apiUrl);
        List<Server> list = new ArrayList<>();
        list.add(defaultServer);
        return list;
    }

}
