## Development env setup

* Need to change datasource details according to your mysql serer in both ``resources/application.yml`` and
  ``resources/META-INF/keyclock-server.json`` file in ``connectionsJpa`` section

* Build Project executing ``mvn clean install``

* And Run Project

* login to autherisation server using following details
  ####url ``http://localhost:9095``
  ####username ``admin``
  ####password ``admin``

* select hyperspace realm and navigate in to
  ``clients/keyclock-service-client/credentials`` click on Regenerate secret.

* Once you copy the generated secret and put it in ``resource server project/application.yml`` as a client secret at
  ``keyclock-client-config:
  client-secret``
