import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CustomValidators} from '../../util/validator/custom-validators';
import Swal from 'sweetalert2';
import {UserService} from '../../service/user.service';
import {User} from '../../model/user.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public signupForm: FormGroup;
  public password: string;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      firstname: new FormControl('',
        [Validators.required, Validators.pattern('[a-zA-Z. ]*')]),
      lastname: new FormControl('',
        [Validators.required, Validators.pattern('[a-zA-Z. ]*')]),
      email: new FormControl('',
        [Validators.required, Validators.email]),
      username: new FormControl('', [Validators.required, Validators.pattern('(?=^.{3,20}$)^[a-zA-Z][a-zA-Z0-9]*?[a-zA-Z0-9]+$')]),
      password: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(8)
      ])),
      confirmPassword: new FormControl('', [Validators.required])
    }, {
      // check whether our password and confirm password match
      validator: CustomValidators.passwordMatchValidator
    });
  }

  get validation(): any {
    return this.signupForm.controls;
  }

  submit(): any {
    if (this.checkValidity()) {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
      });

      swalWithBootstrapButtons.fire({
        title: 'Are you sure',
        text: 'Do you want to submit the details ?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Save it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          const user = new User();
          user.firstName = this.getFirstName();
          user.lastName = this.getLastName();
          user.email = this.getEmail();
          user.username = this.getusername();
          user.password = this.getPassword();
          this.save(user);
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'User not saved :)',
            'error'
          );
        }
      });
    }
  }

  save(user: User): any {
    this.userService.createUser(user).subscribe(
      (response: any) => {
        Swal.fire('Success', 'Details submitted successfully', 'success');
        this.router.navigate(['/login']);
      }, error => {
        console.log('Error', error);
        Swal.fire('User Saving Failed', error.error.message, 'error');
      }
    );
  }

  checkValidity(): any {
    this.signupForm.markAllAsTouched();

    if (this.signupForm.get('firstname').valid &&
      this.signupForm.get('lastname').valid &&
      this.signupForm.get('email').valid &&
      this.signupForm.get('username').valid &&
      this.signupForm.get('password').valid &&
      this.signupForm.get('confirmPassword').valid) {
      return true;
    } else {
      return false;
    }
  }

  getFirstName(): string {
    return this.signupForm.get('firstname').value;
  }

  getLastName(): string {
    return this.signupForm.get('lastname').value;
  }

  getEmail(): string {
    return this.signupForm.get('email').value;
  }

  getusername(): string {
    return this.signupForm.get('username').value;
  }

  getPassword(): string {
    return this.signupForm.get('password').value;
  }

  getConfirmPassword(): string {
    return this.signupForm.get('confirmPassword').value;
  }

  logout(): any {
    this.router.navigate(['/login']);
  }
}
