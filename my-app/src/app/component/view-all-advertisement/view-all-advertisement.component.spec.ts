import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllAdvertisementComponent } from './view-all-advertisement.component';

describe('ViewAllAdvertisementComponent', () => {
  let component: ViewAllAdvertisementComponent;
  let fixture: ComponentFixture<ViewAllAdvertisementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAllAdvertisementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllAdvertisementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
