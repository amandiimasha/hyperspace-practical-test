import { Component, OnInit } from '@angular/core';
import {Advertisement} from '../../model/advertisement.model';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AdvertisementService} from '../../service/advertisement.service';
import {CategoryService} from '../../service/category.service';
import {Category} from '../../model/category.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-all-advertisement',
  templateUrl: './view-all-advertisement.component.html',
  styleUrls: ['./view-all-advertisement.component.scss']
})
export class ViewAllAdvertisementComponent implements OnInit {

  public viewallAdvertisementForm: FormGroup;
  public viewAdvertisementForm: FormGroup;
  public advertisements: Advertisement[] = [];
  public advertisement: Advertisement;
  public selectedAdvertisement: Advertisement;
  public categoryID: string;
  public view: boolean;
  public categories: Category[] = [];
  page = 1;
  pageSize = 5;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private advertisementService: AdvertisementService,
              private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    this.viewallAdvertisementForm = this.formBuilder.group({
      category: new FormControl('',
        [Validators.required])
    });

    this.getAllCategories();
    this.categoryID = this.getCategory();
    this.view = false;
  }

  get validation(): any {
    return this.viewallAdvertisementForm.controls;
  }

  // get all categories
  getAllCategories(): any {
    this.categoryService.getAll().subscribe(
      (response: any) => {
        this.categories = response.data;
      });
  }

  // load advertisements regard selected category
  getAllAdvertisements(categoryID): any {
    this.advertisementService.getAllUsingCategory(categoryID).subscribe(
      (response: any) => {
        if (response.data.length !== 0){
          this.advertisements = response.data;
        } else {
          this.advertisements = [];
        }
      });
  }

  // view advertisement detail
  clickViewBtn(advertisement: Advertisement): any {
    this.view = true;
    this.selectedAdvertisement = advertisement;

    this.viewAdvertisementForm = this.formBuilder.group({
      title: new FormControl(advertisement.title,
        [Validators.required, Validators.maxLength(100)]),
      imageurl: new FormControl(advertisement.imageUrl,
        [Validators.required]),
      category: new FormControl(advertisement.category.id,
        [Validators.required]),
      content: new FormControl(advertisement.description,
        [Validators.required, Validators.maxLength(500)]),
      time: new FormControl(new Date(advertisement.time),
        [Validators.required])
    });
  }

  getCategory(): string {
    return this.viewallAdvertisementForm.get('category').value;
  }

  logout(): any {
    console.log('EEEEEEEEEEE');
    this.router.navigate(['/login']);
  }
}
