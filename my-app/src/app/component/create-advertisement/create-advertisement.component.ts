import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../service/user.service';
import {CustomValidators} from '../../util/validator/custom-validators';
import Swal from 'sweetalert2';
import {User} from '../../model/user.model';
import {Advertisement} from '../../model/advertisement.model';
import {AdvertisementService} from '../../service/advertisement.service';
import {Category} from '../../model/category.model';
import {CategoryService} from '../../service/category.service';

@Component({
  selector: 'app-create-advertisement',
  templateUrl: './create-advertisement.component.html',
  styleUrls: ['./create-advertisement.component.scss']
})
export class CreateAdvertisementComponent implements OnInit {

  public createAdvertisementForm: FormGroup;
  public password: string;
  public advertisements: Advertisement[] = [];
  public selectedAdvertisement: Advertisement;
  page = 1;
  pageSize = 5;
  public categories: Category[] = [];
  public loginuser: string;
  public now: string;
  public view: boolean;
  public edit: boolean;
  // public isRegisteredUser: boolean;
  // public user: string;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private advertisementService: AdvertisementService,
              private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    this.createAdvertisementForm = this.formBuilder.group({
      title: new FormControl('',
        [Validators.required, Validators.maxLength(100)]),
      imageurl: new FormControl('',
        [Validators.required]),
      category: new FormControl('',
        [Validators.required]),
      content: new FormControl('',
        [Validators.required, Validators.maxLength(500)]),
      time: new FormControl('',
        [Validators.required])
    });

    this.getAllCategories();
    this.setTime();
    this.getAllAdvertisements();
    // this.isRegisteredUser = false;

    // if () {
    //   this.isRegisteredUser = true;
    //   this.user = ;
    // } else {
    //   this.isRegisteredUser = false;
    //   this.user = 'Guest User';
    // }
  }

  get validation(): any {
    return this.createAdvertisementForm.controls;
  }

  // get all categories
  getAllCategories(): any {
    this.categoryService.getAll().subscribe(
      (response: any) => {
        this.categories = response.data;
      });
  }

  // get all advertisements of each login users
  getAllAdvertisements(): any {
    this.advertisementService.getAll().subscribe(
      (response: any) => {
        this.advertisements = response.data;
        console.log('AdDDD', this.advertisements);
      });
  }

  setTime(): any{
    setInterval(() => {
      this.now = new Date().toString().split(' ')[4];
    }, 1);
  }

  // create advertisement
  createAdvertisement(): any {
    if (this.checkValidity()) {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
      });

      swalWithBootstrapButtons.fire({
        title: 'Are you sure',
        text: 'Do you want to submit the details ?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Save it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          const advertisement = new Advertisement();
          advertisement.title = this.getTitle();
          advertisement.imageUrl = this.getImageURL();
          // advertisement.category = this.getCategory();
          advertisement.description = this.getContent();
          advertisement.time = new Date();

          //// create advertisement as a logeduser
          // if(login){
          //   advertisement.logUserID = ;
          // }

          this.save(this.getCategory(), advertisement);
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Advertisement not created :)',
            'error'
          );
        }
      });
    }
  }

  save(categoryID: string, advertisement: Advertisement): any {
    this.advertisementService.createAdvertisement(categoryID, advertisement).subscribe(
      (response: any) => {
          Swal.fire('Success', 'Details submitted successfully', 'success');
          this.resetForm();
      }, error => {
        Swal.fire('Advertisement Creating Failed', '', 'error');
      }
    );
  }

  // update advertisement
  updateAdvertisement(): any {
    if (this.checkValidity()) {
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: true
        });

        swalWithBootstrapButtons.fire({
          title: 'Are you sure',
          text: 'Do you want to update the details ?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, Update it!',
          cancelButtonText: 'No, cancel!',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            console.log('Status Co >>');

            const advertisement = new Advertisement();
            advertisement.id = this.selectedAdvertisement.id;
            advertisement.title = this.getTitle();
            advertisement.imageUrl = this.getImageURL();
            // advertisement.category = this.getCategory();
            advertisement.description = this.getContent();
            advertisement.time = new Date();

            this.update(this.getCategory(), advertisement);
          }
        });
    }
  }

  update(categoryID: string, advertisement: Advertisement): any {
    this.advertisementService.updateAdvertisement(categoryID, advertisement).subscribe(
      (data: any) => {
        Swal.fire('Success', 'Updated successfully', 'success');
        this.resetForm();
      }, (err: any) => {
        Swal.fire('Something went wrong...!', err.message, 'error');
      }
    );
  }

  checkValidity(): any {
    this.createAdvertisementForm.get('title').markAllAsTouched();
    this.createAdvertisementForm.get('imageurl').markAllAsTouched();
    this.createAdvertisementForm.get('category').markAllAsTouched();
    this.createAdvertisementForm.get('content').markAllAsTouched();

    if (this.createAdvertisementForm.get('title').valid &&
      this.createAdvertisementForm.get('imageurl').valid &&
      this.createAdvertisementForm.get('category').valid &&
      this.createAdvertisementForm.get('content').valid) {
      return true;
    } else {
      return false;
    }
  }

  getTitle(): string {
    return this.createAdvertisementForm.get('title').value;
  }

  getImageURL(): string {
    return this.createAdvertisementForm.get('imageurl').value;
  }

  getCategory(): string {
    return this.createAdvertisementForm.get('category').value;
  }

  getContent(): string {
    return this.createAdvertisementForm.get('content').value;
  }

  resetForm(): any{
    this.view = false;
    this.edit = false;
    this.createAdvertisementForm = this.formBuilder.group({
      title: new FormControl('',
        [Validators.required, Validators.maxLength(100)]),
      imageurl: new FormControl('',
        [Validators.required]),
      category: new FormControl('',
        [Validators.required]),
      content: new FormControl('',
        [Validators.required, Validators.maxLength(500)]),
      time: new FormControl('',
        [Validators.required])
    });

    this.getAllAdvertisements();
  }

  // update advertisement detail
  clickEditBtn(advertisement: Advertisement): any{
    this.edit = true;
    this.view = false;
    this.selectedAdvertisement = advertisement;

    this.createAdvertisementForm = this.formBuilder.group({
      title: new FormControl(advertisement.title,
        [Validators.required, Validators.maxLength(100)]),
      imageurl: new FormControl(advertisement.imageUrl,
        [Validators.required]),
      category: new FormControl(advertisement.category.id,
        [Validators.required]),
      content: new FormControl(advertisement.description,
        [Validators.required, Validators.maxLength(500)]),
      time: new FormControl(new Date(advertisement.time),
        [Validators.required])
    });
  }

  // view advertisement detail
  clickViewBtn(advertisement: Advertisement): any {
    this.view = true;
    this.edit = false;
    this.selectedAdvertisement = advertisement;

    this.createAdvertisementForm = this.formBuilder.group({
      title: new FormControl(advertisement.title,
        [Validators.required, Validators.maxLength(100)]),
      imageurl: new FormControl(advertisement.imageUrl,
        [Validators.required]),
      category: new FormControl(advertisement.category.id,
        [Validators.required]),
      content: new FormControl(advertisement.description,
        [Validators.required, Validators.maxLength(500)]),
      time: new FormControl(new Date(advertisement.time),
        [Validators.required])
    });
  }

  // delete advertisement detail
  clickDeleteBtn(advertisement: Advertisement): any {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure',
      text: 'Do you want to delete the details ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.advertisementService.deleteAdvertisement(String(advertisement.category.id), String(advertisement.id)).subscribe(
          (response: any) => {
            console.log('Delete', response);
            Swal.fire('Success', 'Deleted advertisement', 'success');
            this.resetForm();
          }, (err: any) => {
            Swal.fire('Something went wrong...!', '', 'error');
          }
        );
      }});
  }

  // redirect to the advertisement page
  allAdvertisements(): any {
    this.router.navigate(['/viewall']);
  }

  logout(): any {
    this.router.navigate(['/login']);
  }
}
