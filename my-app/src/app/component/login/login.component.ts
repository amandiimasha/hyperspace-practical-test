import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public errorMessage = true;
  public inactiveUserMessage = true;
  public mode: string;

  public show = false;
  public password = 'password';

  constructor(private router: Router,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  get validation(): any {
    return this.loginForm.controls;
  }

  public submitLoginForm(): any {
    if (this.checkValidity()) {
      this.router.navigate(['/create']);
    }

    // if (this.checkValidity()) {
    //   this.authService.login(this.getUserName(), this.getPassword()).subscribe(
    //     (response) => {
    //       if (response.code === 200){
    //         this.router.navigate(['/transaction-logs']);
    //       }else{
    //         this.errorMessage = false;
    //       }
    //     },
    //     (error) => {
    //       this.errorMessage = false;
    //     });
    // }
  }

  getUserName(): any {
    return this.loginForm.get('username').value;
  }

  getPassword(): any {
    return this.loginForm.get('password').value;
  }

  onClick(): any {
    if (this.password === 'password') {
      this.password = 'text';
      this.show = true;
    } else {
      this.password = 'password';
      this.show = false;
    }
  }

  checkValidity(): boolean {
    this.loginForm.get('username').markAsTouched();
    this.loginForm.get('password').markAsTouched();

    if (this.loginForm.get('username').valid &&
      this.loginForm.get('password').valid){
      return true;
    } else {
      return false;
    }
  }
}
