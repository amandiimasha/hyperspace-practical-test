import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './auth/auth.guard';
import {LoginComponent} from './component/login/login.component';
import {ErrorPageComponent} from './component/error-page/error-page.component';
import {SignupComponent} from './component/signup/signup.component';
import {CreateAdvertisementComponent} from './component/create-advertisement/create-advertisement.component';
import {ViewAllAdvertisementComponent} from './component/view-all-advertisement/view-all-advertisement.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'create',
    component: CreateAdvertisementComponent
  },
  {
    path: 'viewall',
    component: ViewAllAdvertisementComponent
  },
  {
    path: 'error',
    component: ErrorPageComponent
    // canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
