import {Category} from './category.model';

export class Advertisement {
  public id: number;
  public title: string;
  public imageUrl: string;
  public category: Category;
  public description: string;
  public time: Date;
  // public logUserID: string;
}
