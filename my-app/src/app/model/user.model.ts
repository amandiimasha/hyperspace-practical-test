export class User {
  public id: number;
  public firstName: string;
  public lastName: string;
  public email: string;
  public username: string;
  public password: string;
  //
  // public access_token?: string;
  // public refresh_token?: string;
  // public expires_in: bigint;
  // public scope: string;
}
