import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './component/login/login.component';
import {ErrorPageComponent} from './component/error-page/error-page.component';
import {SignupComponent} from './component/signup/signup.component';
import {CreateAdvertisementComponent} from './component/create-advertisement/create-advertisement.component';
import {ViewAllAdvertisementComponent} from './component/view-all-advertisement/view-all-advertisement.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ErrorPageComponent,
    SignupComponent,
    CreateAdvertisementComponent,
    ViewAllAdvertisementComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
