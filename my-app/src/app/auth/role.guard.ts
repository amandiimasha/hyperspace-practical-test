import {Injectable} from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree, Router
} from '@angular/router';
import {Observable} from 'rxjs';
import {RoleService} from './role.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  accessUserRole: string[] = [];
  currentUserRole: string[] = [];
  activate = false;

  constructor(public roleService: RoleService, public router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    this.currentUserRole = JSON.parse(localStorage.getItem('currentUserRole'));
    this.accessUserRole = route.data.roles;

    this.currentUserRole.forEach(role => {
      this.accessUserRole.forEach(accessUser => {
        if (role === accessUser) {
          this.activate = true;
        }
      });
    });

    if (!this.activate) {
      this.router.navigate(['/error']);
    }

    return this.activate;
    // if (route.data.roles.(localStorage.getItem('currentUser')) === -1) {
    //   this.activate = false;
    //   // this.router.navigate(['/login']);
    //   // route.data.roles.foreach(role => {
    //   //   if (role === localStorage.getItem('currentUser')) {
    //   //     this.activate = false;
    //   //   } else {
    //   //     this.activate = true;
    //   //   }
    //   // });
    // } else {
    //   this.activate = true;
    // }
  }
}
