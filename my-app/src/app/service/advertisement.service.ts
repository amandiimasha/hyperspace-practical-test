import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Advertisement} from '../model/advertisement.model';

@Injectable({
  providedIn: 'root'
})
export class AdvertisementService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  createAdvertisement(categoryID: string, advertisement: Advertisement): Observable<any> {
    return this.http.post(environment.api_url + '/categories/' + categoryID + '/advertisings', JSON.stringify(advertisement), this.httpOptions);
  }

  deleteAdvertisement(categoryID: string, advertisementID: string): Observable<any> {
    console.log('Delete');
    return this.http.delete(environment.api_url + '/categories/' + categoryID + '/advertisings/' + advertisementID);
  }

  getAllUsingCategory(categoryID: string): Observable<any> {
    return this.http.get(environment.api_url + '/categories/' + categoryID + '/advertisings');
  }

  getAll(): Observable<any> {
    console.log('geeeeeeeeeeeeeeet');
    return this.http.get(environment.api_url + '/advertisings');
  }

  getAdvertisements(categoryID: string, advertisementID: string): Observable<any> {
    console.log(categoryID);
    console.log(advertisementID);
    return this.http.get(environment.api_url + '/categories/' + categoryID + '/advertisings/' + advertisementID);
  }

  searchAdvertisement(keyword: string, selectedPageNo: number, limit: number): Observable<any> {
    return this.http.get('');
  }

  updateAdvertisement(id: any, advertisement: Advertisement): Observable<any> {
    console.log('Update', advertisement);
    console.log('Update', id);
    return this.http.put(environment.api_url + '/categories/' + id + '/advertisings/' + advertisement.id, JSON.stringify(advertisement), this.httpOptions);
  }
}
