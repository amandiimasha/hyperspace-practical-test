import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  createUser(user): Observable<any> {
    console.log(user);
    return this.http.post(environment.api_url + '/users', JSON.stringify(user), this.httpOptions);
  }

  deleteUser(userID): Observable<any> {
    return this.http.delete(environment.api_url + '/users/' + userID);
  }

  getAll(): Observable<any> {
    return this.http.get(environment.api_url + '/users');
  }

  getUsers(number1: number, number2: number): Observable<any> {
    return this.http.get(environment.api_url + '/users');
  }

  searchUser(keyword: string, selectedPageNo: number, limit: number): Observable<any> {
    return null;
    // return this.http.get('');
  }

  updateUser(id: any, user): Observable<any> {
    return this.http.post(environment.api_url + '/users/' + id, JSON.stringify(user), this.httpOptions);
  }
}
