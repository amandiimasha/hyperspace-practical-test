import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(environment.api_url + '/categories');
  }

  getCategoryById(categoryID: string): Observable<any> {
    return this.http.get(environment.api_url + '/categories/' + categoryID);
  }

  getCategories(number1: number, number2: number): Observable<any> {
    return this.http.get(environment.api_url + '/categories');
  }
}
